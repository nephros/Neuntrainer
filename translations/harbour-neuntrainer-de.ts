<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="22"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>What&apos;s %1?</source>
        <translation>Was ist %1?</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="27"/>
        <source>%1 lets you add words to (&quot;train&quot;) the Xt9 input prediction engine that comes with licensed versions of Sailfish OS.</source>
        <translation>Mit %1 kannst du Wörter dem Xt9 System hinzufügen (&quot;antrainieren&quot;). Xt9 ist auf lizensierten Versionen von Sailfish OS verfügbar.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Copyright:</source>
        <translation>Urheberrecht:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>License:</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Source Code:</source>
        <translation>Quellcode</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Übersetzung: %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>Loading failed</source>
        <translation>Laden fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>Not a Text file.</source>
        <translation>Keine Text-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>File empty or not accessible.</source>
        <translation>Datei ist leer, oder konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="68"/>
        <source>Select text file</source>
        <translation>Wähle eine Text-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Training language: %1</source>
        <translation>Zielsprache</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="151"/>
        <source>Strip punctuation</source>
        <translation>Entferne Sonderzeichen</translation>
    </message>
    <message>
        <source>If enabled, characters that are not letters are removed before learning.</source>
        <translation type="vanished">Wenn aktiv werden alle Zeichen die keine Buchstaben sind vor dem Lernen entfernt.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="133"/>
        <source>Learn selected</source>
        <translation>Auswahl lernen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="152"/>
        <source>If enabled, characters that are not letters are removed before learning. Turn this off if you want to learn things like URLs or words with special characters.</source>
        <translation>Wenn aktiv werden alle Zeichen die keine Buchstaben sind vor dem Lernen entfernt. Schaltes es aus, wenn du Web-Adressen oder Worte mit Sonderzeichen trainieren willst.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="186"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="187"/>
        <source>Switch Language</source>
        <translation>Sprache ändern</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="188"/>
        <source>Load File…</source>
        <translation>Datei laden…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="236"/>
        <source>learned: </source>
        <translation>gelernt: </translation>
    </message>
    <message>
        <source>Current language: %1</source>
        <translation type="vanished">Aktuelle Sprache: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="100"/>
        <source>Target Language</source>
        <translation>Zielsprache</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Language that will be trained</source>
        <translation>Sprache, die trainiert wird</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="138"/>
        <source>Learn all words</source>
        <translation>Alle Wörter lernen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="145"/>
        <source>Learning all words</source>
        <translation>Lerne alle Wörter</translation>
    </message>
</context>
</TS>
