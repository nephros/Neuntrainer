<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="22"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>What&apos;s %1?</source>
        <translation>Vad är %1?</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="27"/>
        <source>%1 lets you add words to (&quot;train&quot;) the Xt9 input prediction engine that comes with licensed versions of Sailfish OS.</source>
        <translation>%1 låter dig lägga till ord (träna) i den Xt9 textförutsägelsemotor (Predictive text input) som kan installeras i licensierade versioner av Sailfish OS.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="34"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>License:</source>
        <translation>Licens:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="37"/>
        <source>Source Code:</source>
        <translation>Källkod:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>Credits</source>
        <translation>Erkännanden</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Översättningar: %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>Loading failed</source>
        <translation>Inläsning misslyckades</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>Not a Text file.</source>
        <translation>Ingen textfil.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>File empty or not accessible.</source>
        <translation>Filen är tom eller inte tillgänglig.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="68"/>
        <source>Select text file</source>
        <translation>Välj en textfil</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Training language: %1</source>
        <translation>Tränar språk: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="151"/>
        <source>Strip punctuation</source>
        <translation>Ta bort skiljetecken</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="133"/>
        <source>Learn selected</source>
        <translation>Mata in markerat</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="152"/>
        <source>If enabled, characters that are not letters are removed before learning. Turn this off if you want to learn things like URLs or words with special characters.</source>
        <translation>Om detta aktiveras, tas tecken som inte är bokstäver bort, innan de matas in. Stäng av detta om du vill mata in saker som webbadresser, eller ord med specialtecken.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="186"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="187"/>
        <source>Switch Language</source>
        <translation>Byt språk</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="188"/>
        <source>Load File…</source>
        <translation>Läs in fil...</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="236"/>
        <source>learned: </source>
        <translation>Inmatat: </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="100"/>
        <source>Target Language</source>
        <translation>Målspråk</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="101"/>
        <source>Language that will be trained</source>
        <translation>Språk som kommer att tränas</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="138"/>
        <source>Learn all words</source>
        <translation>Mata in alla ord</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="145"/>
        <source>Learning all words</source>
        <translation>Matar in alla ord</translation>
    </message>
</context>
</TS>
