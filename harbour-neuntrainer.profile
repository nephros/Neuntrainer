# -*- mode: sh -*-

# Firejail profile for /usr/bin/harbour-neuntrainer

# x-sailjail-translation-catalog = harbour-neuntrainer
# x-sailjail-translation-key-description = permission-la-data
# x-sailjail-description = XT9 socket access
# x-sailjail-translation-key-long-description = permission-la-data_description
# x-sailjail-long-description = Store learned words

### PERMISSIONS
# x-sailjail-permission = MediaIndexing
# x-sailjail-permission = Documents

whitelist ${RUNUSER}/xt9.sock
