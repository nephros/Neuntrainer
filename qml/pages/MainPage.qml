/*

MIT License

Copyright (c) 2022 Peter G. (nephros)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

import QtQuick 2.6
import com.jolla.xt9 1.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import Nemo.Notifications 1.0
import "lang.js" as Languages

Page {
    id: page

    property string language: settings.language ? settings.language : Qt.locale().name.substr(0,2).toUpperCase()
    property string selectedDocument
    property string mimeType

    // wire up pasting from Cover
    Component.onCompleted: {
        app.coverPasted.connect(pasteFromClip)
    }
    function pasteFromClip() { if (Clipboard.hasText) srcField.text = Clipboard.text }

    onLanguageChanged: {
        settings.language = language
    }

    onSelectedDocumentChanged: {
        if (selectedDocument.length > 0) {
            const success = (loadFile(selectedDocument, mimeType) === 0);
            if (!success) {
                notification.summary = qsTr("Loading failed") + ": " + qsTr("Not a Text file.")
                notification.publish();
            }
        } else {
            notification.summary = qsTr("Loading failed") + ": " + qsTr("File empty or not accessible.")
            notification.publish();
            return
        }
    }

    Component {
        id: documentPickerPage
        DocumentPickerPage {
            title: qsTr("Select text file")
            onSelectedContentPropertiesChanged: {
                page.mimeType = selectedContentProperties.mimeType;
                page.selectedDocument = selectedContentProperties.url;
            }
        }
    }

    ListModel { id: langModel
        Component.onCompleted: {
            Languages.data.forEach(function(o) { append(o); });
        }
    }

    ListModel { id: logModel }

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column { id: col
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeLarge
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            PageHeader { id: head ; title: qsTr(Qt.application.name); description: qsTr("Training language: %1").arg(language) }
            ComboBox { id: langSelector
                visible: page.language == ""
                width: parent.width
                height: Theme.itemSizeMedium
                label: qsTr("Target Language")
                description: qsTr("Language that will be trained")
                currentIndex: -1
                menu: ContextMenu { Repeater {
                    model: langModel
                    delegate: MenuItem { text: modelData }
                }}
                onValueChanged: if (value !== "") { page.language = value; visible=false }
                function reset() {
                    page.language = "";
                    visible = true;
                    currentIndex = -1;
                }
            }
            TextArea { id: srcField
                enabled: language !== ""
                width: parent.width
                text: "lorem ipsum dolor sit amet\n\njentacular jentacular jentacular\n\nGeorge Carlin's seven dirty words are: shit, piss, fuck, cunt, cocksucker, motherfucker, and tits."
                //placeholderText: qsTr("Add some words to learn.")
                height: Math.min(Math.max(Theme.itemSizeHuge, implicitHeight), page.height/2)
                focus: true
                softwareInputPanelEnabled: true
                wrapMode: TextEdit.WordWrap
                inputMethodHints: Qt.ImhNoAutoUppercase
                leftItem: Column {
                    spacing: Theme.paddingSmall
                    IconButton { id: clearButton; enabled: language !== ""; icon.source: "image://theme/icon-m-input-remove"; onClicked: srcField.text = "" }
                    IconButton { id: pasteButton; enabled: ((language !== "") && (Clipboard.hasText)); icon.source: "image://theme/icon-m-clipboard"; onClicked: srcField.text = Clipboard.text }
                }
            }
            ButtonLayout { id: buttons
                anchors.horizontalCenter: parent.horizontalCenter
                Button { id: learnSelected
                    text: qsTr("Learn selected")
                    enabled: ((language !== "") && (srcField.selectedText !== ""))
                    onClicked: { srcField.copy(); learn(Clipboard.text, settings.strip); }
                }
                Button { id: learnAllButton
                    text: qsTr("Learn all words")
                    enabled: language !== ""
                    onClicked: {
                        srcField.selectAll();
                        srcField.copy();
                        srcField.deselect();
                        if (!Clipboard.hasText) return;
                        remorse.execute(qsTr("Learning all words"), function() { learnAll(Clipboard.text, settings.strip) }, 3500)
                    }
                }
            }
            TextSwitch {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Strip punctuation")
                description: qsTr("If enabled, characters that are not letters are removed before learning. Turn this off if you want to learn things like URLs or words with special characters.")
                checked: settings.strip
                onCheckedChanged: settings.strip = checked
            }
            ListView { id: logview
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width -  Theme.horizontalPageMargin * 2
                height: Theme.itemSizeHuge
                spacing: 0
                visible: logModel.count > 0
                clip: true
                add:      Transition { FadeAnimation { duration: 600 } }
                move:     Transition { FadeAnimation { duration: 600 } }
                populate: Transition { FadeAnimation { duration: 600 } }
                model: logModel
                highlightFollowsCurrentItem: true
                onCountChanged: currentIndex = count - 1
                highlight: Component { Rectangle { color: Theme.rgba(Theme.secondaryHighlightColor, Theme.opacityFaint); } }
                delegate: Component { Label {
                    clip: true
                    text: logline
                    wrapMode: TextEdit.WordWrap
                    color: Theme.secondaryColor
                    font.family: "monospace"
                    font.pixelSize: Theme.fontSizeTiny
                }
                }
            }

        }
    }

    PullDownMenu {
        flickable: flick
        MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
        MenuItem { enabled: !langSelector.visible; text: qsTr("Switch Language"); onClicked: { langSelector.reset() } }
        MenuItem { text: qsTr("Load File…"); onClicked: pageStack.push(documentPickerPage) }
    }

    RemorsePopup { id: remorse }
    Notification { id: notification; isTransient: true; appName: qsTr(Qt.application.name); category: "transfer.error"}
    // timer and function, used to emulate a sleep in loops (see learn())
    Timer { id: delayTimer
        property string word
        onTriggered: thread.acceptWord(word, false)
    }
    function delayLearn(w,t) {
        delayTimer.interval = t; 
        delayTimer.repeat = true; 
        delayTimer.word = w; 
        delayTimer.start();
    }
    // this saves our learned words:
    Xt9EngineThread { id: thread
        language: page.language
    }

    // XMLHttpRequest to load files
    function loadFile(url, type) {
        console.assert((typeof url !== "undefined"), "Called without filepath");
        console.assert((typeof type !== "undefined"), "Called without type");
        if (/^text\//.test(type)) {
            console.info("Loading " + type + " file from " + url);
        } else {
            console.warn("Not a text file:" + type);
            return 5
        }
        console.time("File request took");
        var r = new XMLHttpRequest()
        r.open('GET', url);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.send();

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                fillTextField(r.response);
                console.timeEnd("File request took");
            }
        }
        return 0 //javascript true would be 1…
    }

    // helper callback to process the response from XMLHttpRequest
    function fillTextField(text) { srcField.text = text }

    function learnAll(words, strip) {
        words.split("\n").forEach(function(l){l.split(" ").forEach(learn, strip)});
    }
    function learn(word, strip) {
        if (word !== "") {
            //const w = strip ? word.replace(/\W+/gi, "") : word;
            const stripre = /[\p{Alpha}\p{M}\p{Nd}\p{Pc}\p{Join_C}]+/gi; // unicode word characters
            const w = strip ? word.replace(stripre, "") : word;
            // https://forum.sailfishos.org/t/q-how-can-i-bring-predictive-text-input-to-learn-a-new-word/10402/6
            // apparently accepting three times learns
            //for ( var i=0; i<3; i++) { thread.acceptWord(w, false); }
            // but the loop fires too quickly(?) so lets delay that:
            for ( var i=0; i<3; i++) { delayLearn(w, i*200); }
            const linetext = qsTr("learned: ") + w
            logModel.append({ "logline": linetext });
            console.info(linetext);
        }
    }
}

/*
 *
 ****************************************
 *    Graveyard
 ****************************************
 *
 * some attempts, worth keeping ofr reference...
 *
 /*
    /*
     * Target Language picking version I:
     * straight from /usr/share/jolla-settings/pages/text_input/textinput.qml
     *
     * Unfortunately reading the DConf value does not work in SailJail
     *
     */
    /*
    LayoutModel {
        id: layoutModel
    }
    // component defined in /usr/share/jolla-settings/pages/text_input, copy it!
    EnabledLayoutModel {
        id: enabledLayoutModel
        layoutModel: layoutModel
        refreshTimer.onTriggered: {
            enabledLayoutModel.updating = false
        }
    }
    function getCurrentLayout() {
        enabledLayoutModel.refresh();
        for (var i = 0; i < layoutModel.count; ++i) {
            var item = layoutModel.get(i)
            if (item.enabled) {
                //console.debug("got: "+ JSON.stringify(item,null,4));
                //var o = {"language": item.lanuageCode, "name": item.name, "layout": item.layout};
                //console.debug("found o: " + o);
                //return o;
                return item.languageCode;
            }
        }
        return null
    }
    property alias currentLayout: currentLayoutConfig.value
    ConfigurationValue { id: currentLayoutConfig
        key: "/sailfish/text_input/active_layout"
    }
    onCurrentLayoutChanged: { language = getCurrentLayout()}
    Component.onCompleted: { language = getCurrentLayout()}
    */

    /*
     * Target Language picking version II: use separate page
     */
    /*
    Component { id: languagePickerPage
        LanguagePickerPage {
            title: qsTr("Select Language")
            onSelectedLanguageChanged: {
                page.language = selectedLanguage
            }
        }
    }
    */


// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
