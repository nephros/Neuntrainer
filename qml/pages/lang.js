/*
 * not all existing languages are supported by xt9, esp. if custom keybosards are installed.
 * This list was found by:
 * rpm -qf /usr/share/maliit/plugins/com/jolla/layouts/*conf | grep jolla-key | xargs rpm -ql | grep .conf | grep \/layouts | xargs grep handler=Xt9Inp -l | xargs grep Code
 *
 * TODO: support the Chinese handler as well
 *
 * this file could be pure JSON, but this way we can do comments and things.
 */

var data = [
    { "languageCode": "BG"},
    { "languageCode": "CS"},
    { "languageCode": "DA"},
    { "languageCode": "DE"},
    { "languageCode": "EL"},
    { "languageCode": "EN"},
    { "languageCode": "ES"},
    { "languageCode": "ET"},
    { "languageCode": "FI"},
    { "languageCode": "FR"},
    { "languageCode": "HU"},
    { "languageCode": "IT"},
    { "languageCode": "LT"},
    { "languageCode": "NL"},
    { "languageCode": "NO"},
    { "languageCode": "PL"},
    { "languageCode": "PT"},
    { "languageCode": "RO"},
    { "languageCode": "RU"},
    { "languageCode": "SK"},
    { "languageCode": "SL"},
    { "languageCode": "SV"},
    { "languageCode": "TR"},
]
