import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
  id: about

    readonly property string copyright: "Peter G. (nephros)"
    readonly property string email: "mailto:sailfish@nephros.org?bcc=sailfish+neuntrainer@nephros.org&subject=A%20message%20from%20a%20" + Qt.application.name + "%20user&body=Hello%20nephros%2C%0A"
    readonly property string license: "MIT"
    readonly property string licenseurl: "https://mit-license.org"
    readonly property string source: "https://codeberg.org/nephros/neuntrainer"

  SilicaFlickable {
    contentHeight: col.height + Theme.itemSizeLarge
    anchors.fill: parent
    VerticalScrollDecorator {}
    Column {
        id: col
        width: parent.width - Theme.horizontalPageMargin
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingLarge
        PageHeader { title: qsTr("About") + " " + Qt.application.name + " " + Qt.application.version }
        SectionHeader { text: qsTr("What's %1?").arg(Qt.application.name) }
        LinkedLabel {
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          text: qsTr('%1 lets you add words to ("train") the Xt9 input prediction engine that comes with licensed versions of Sailfish OS.').arg(Qt.application.name)
          color: Theme.secondaryColor
          font.pixelSize: Theme.fontSizeSmall
          horizontalAlignment: Text.AlignJustify
          wrapMode: Text.WordWrap
          plainText: this.text
        }
        DetailItem { label: qsTr("Version:");      value: Qt.application.version }
        DetailItem { label: qsTr("Copyright:");    value: copyright;                            BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(email) } }
        DetailItem { label: qsTr("License:");      value: license + " (" + licenseurl + ")";    BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(licenseurl) } }
        DetailItem { label: qsTr("Source Code:");  value: source;                               BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(source) } }
        SectionHeader { text: qsTr("Credits") }
        DetailItem { label: qsTr("Translation: %1",  "%1 is the native language name").arg(Qt.locale("de").nativeLanguageName); value: "nephros" }
        DetailItem { label: qsTr("Translation: %1",  "%1 is the native language name").arg(Qt.locale("sv").nativeLanguageName); value: "eson" }
    }
  }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
