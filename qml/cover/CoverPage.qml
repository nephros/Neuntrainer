import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage
    Image {
        source: "./background.png"
        anchors {
            horizontalCenter: parent.right
            bottom: parent.bottom
        }
        //height: parent.height
        width: parent.width
        sourceSize.width: width
        fillMode: Image.PreserveAspectFit
        opacity: 0.33
    }
    Text { id: neun
        text: "neun"
        anchors.centerIn: parent
        font.family:  "monospace"
        font.pixelSize: Theme.fontSizeHuge
        horizontalAlignment: Text.AlignHCenter
        color: Theme.secondaryColor
    }
    Text {
        text: "trainer"
        //anchors.left: neun.right
        anchors.top: neun.verticalCenter
        anchors.right: parent.right
        font.family:  "monospace"
        font.pixelSize: Theme.fontSizeHuge
        color: Theme.primaryColor
    }
    CoverActionList {
        CoverAction {id: pasteAction
            iconSource: "image://theme/icon-l-clipboard"
            onTriggered: { app.activate() }
            Component.onCompleted: pasteAction.triggered.connect(app.coverPasted)
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
