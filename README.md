## Neuntrainer

A little app for Sailfish OS which allows adding words to the XT9 prediction.

You can get it from Sailfishos Chum (best via the app, or at [3]).  
You need a licensed version of Sailfish OS to install this, as it depends on the XT9 feature.

Inspired by the thread about the same ([1]), and Pekka Vuorela's suggestion at [2].

 > Learning words needs to go through the prediction library. Only interface at the moment is the qml one. If you want to hack for yourself, guess you can check Xt9InputHandler.qml and do some simple app that does acceptWord() parts.


**NOTE**: it is unknown whether adding a large number of words to the database has a negative effect in some way. Use reponsibly.  
After all this is input text prediction, not a thesaurus or dictionary. If you need one of these, use one of these.



#### Features/use cases supported at the moment:

- Entering a couple of words in the text field
- Selecting a word in the text field and learning it
- Copypasting a phrase you have in the clipboard into the text field, and learning all words in it
- Loading a text file from your phone and learning the words in it


#### Features that won't be added (by me, Pull Requests are always welcome):

 - loading things from the net
 - better target language selection: I had an implementation which could detect target language from the current keyboard, but this does not work in SailJail so I dropped it

### References

[1] https://forum.sailfishos.org/t/q-how-can-i-bring-predictive-text-input-to-learn-a-new-word/10402  
[2] https://together.jolla.com/question/143953/question-quick-adding-a-list-of-words-as-a-learned-words-for-custom-vkb/  
[3] https://build.sailfishos.org/package/show/sailfishos:chum/neuntrainer  

### License

licensed under the [MIT License](https://mit-license.org)