######################################################################
# This is for translation ONLY, use build.sh for building
######################################################################

TEMPLATE = aux
TARGET = harbour-neuntrainer
CONFIG += sailfishapp sailfishapp_i18n

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    qml/pages/AboutPage.qml \
    qml/cover/CoverPage.qml \
    qml/pages/LanguagePickerPage.qml \
    qml/pages/MainPage.qml

}

# Input
TRANSLATIONS += translations/$${TARGET}-en.ts \
                translations/$${TARGET}-de.ts \
                translations/$${TARGET}-sv.ts
